/**
  @Generated Pin Manager Header File

  @Company:
    Microchip Technology Inc.

  @File Name:
    pin_manager.h

  @Summary:
    This is the Pin Manager file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  @Description
    This header file provides APIs for driver for .
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.78
        Device            :  PIC18F45K50
        Driver Version    :  2.11
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.05 and above
        MPLAB 	          :  MPLAB X 5.20	
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#ifndef PIN_MANAGER_H
#define PIN_MANAGER_H

/**
  Section: Included Files
*/

#include <xc.h>

#define INPUT   1
#define OUTPUT  0

#define HIGH    1
#define LOW     0

#define ANALOG      1
#define DIGITAL     0

#define PULL_UP_ENABLED      1
#define PULL_UP_DISABLED     0


// get/set KLED_NUM aliases
#define KLED_NUM_TRIS                 TRISAbits.TRISA0
#define KLED_NUM_LAT                  LATAbits.LATA0
#define KLED_NUM_PORT                 PORTAbits.RA0
#define KLED_NUM_ANS                  ANSELAbits.ANSA0
#define KLED_NUM_SetHigh()            do { LATAbits.LATA0 = 1; } while(0)
#define KLED_NUM_SetLow()             do { LATAbits.LATA0 = 0; } while(0)
#define KLED_NUM_Toggle()             do { LATAbits.LATA0 = ~LATAbits.LATA0; } while(0)
#define KLED_NUM_GetValue()           PORTAbits.RA0
#define KLED_NUM_SetDigitalInput()    do { TRISAbits.TRISA0 = 1; } while(0)
#define KLED_NUM_SetDigitalOutput()   do { TRISAbits.TRISA0 = 0; } while(0)
#define KLED_NUM_SetAnalogMode()      do { ANSELAbits.ANSA0 = 1; } while(0)
#define KLED_NUM_SetDigitalMode()     do { ANSELAbits.ANSA0 = 0; } while(0)

// get/set KLED_CAPS aliases
#define KLED_CAPS_TRIS                 TRISAbits.TRISA1
#define KLED_CAPS_LAT                  LATAbits.LATA1
#define KLED_CAPS_PORT                 PORTAbits.RA1
#define KLED_CAPS_ANS                  ANSELAbits.ANSA1
#define KLED_CAPS_SetHigh()            do { LATAbits.LATA1 = 1; } while(0)
#define KLED_CAPS_SetLow()             do { LATAbits.LATA1 = 0; } while(0)
#define KLED_CAPS_Toggle()             do { LATAbits.LATA1 = ~LATAbits.LATA1; } while(0)
#define KLED_CAPS_GetValue()           PORTAbits.RA1
#define KLED_CAPS_SetDigitalInput()    do { TRISAbits.TRISA1 = 1; } while(0)
#define KLED_CAPS_SetDigitalOutput()   do { TRISAbits.TRISA1 = 0; } while(0)
#define KLED_CAPS_SetAnalogMode()      do { ANSELAbits.ANSA1 = 1; } while(0)
#define KLED_CAPS_SetDigitalMode()     do { ANSELAbits.ANSA1 = 0; } while(0)

// get/set KLED_SCROLL aliases
#define KLED_SCROLL_TRIS                 TRISAbits.TRISA2
#define KLED_SCROLL_LAT                  LATAbits.LATA2
#define KLED_SCROLL_PORT                 PORTAbits.RA2
#define KLED_SCROLL_ANS                  ANSELAbits.ANSA2
#define KLED_SCROLL_SetHigh()            do { LATAbits.LATA2 = 1; } while(0)
#define KLED_SCROLL_SetLow()             do { LATAbits.LATA2 = 0; } while(0)
#define KLED_SCROLL_Toggle()             do { LATAbits.LATA2 = ~LATAbits.LATA2; } while(0)
#define KLED_SCROLL_GetValue()           PORTAbits.RA2
#define KLED_SCROLL_SetDigitalInput()    do { TRISAbits.TRISA2 = 1; } while(0)
#define KLED_SCROLL_SetDigitalOutput()   do { TRISAbits.TRISA2 = 0; } while(0)
#define KLED_SCROLL_SetAnalogMode()      do { ANSELAbits.ANSA2 = 1; } while(0)
#define KLED_SCROLL_SetDigitalMode()     do { ANSELAbits.ANSA2 = 0; } while(0)

// get/set KLED_CHIAVE aliases
#define KLED_CHIAVE_TRIS                 TRISAbits.TRISA3
#define KLED_CHIAVE_LAT                  LATAbits.LATA3
#define KLED_CHIAVE_PORT                 PORTAbits.RA3
#define KLED_CHIAVE_ANS                  ANSELAbits.ANSA3
#define KLED_CHIAVE_SetHigh()            do { LATAbits.LATA3 = 1; } while(0)
#define KLED_CHIAVE_SetLow()             do { LATAbits.LATA3 = 0; } while(0)
#define KLED_CHIAVE_Toggle()             do { LATAbits.LATA3 = ~LATAbits.LATA3; } while(0)
#define KLED_CHIAVE_GetValue()           PORTAbits.RA3
#define KLED_CHIAVE_SetDigitalInput()    do { TRISAbits.TRISA3 = 1; } while(0)
#define KLED_CHIAVE_SetDigitalOutput()   do { TRISAbits.TRISA3 = 0; } while(0)
#define KLED_CHIAVE_SetAnalogMode()      do { ANSELAbits.ANSA3 = 1; } while(0)
#define KLED_CHIAVE_SetDigitalMode()     do { ANSELAbits.ANSA3 = 0; } while(0)

// get/set SERVICE01_DBG aliases
#define SERVICE01_DBG_TRIS                 TRISAbits.TRISA4
#define SERVICE01_DBG_LAT                  LATAbits.LATA4
#define SERVICE01_DBG_PORT                 PORTAbits.RA4
#define SERVICE01_DBG_SetHigh()            do { LATAbits.LATA4 = 1; } while(0)
#define SERVICE01_DBG_SetLow()             do { LATAbits.LATA4 = 0; } while(0)
#define SERVICE01_DBG_Toggle()             do { LATAbits.LATA4 = ~LATAbits.LATA4; } while(0)
#define SERVICE01_DBG_GetValue()           PORTAbits.RA4
#define SERVICE01_DBG_SetDigitalInput()    do { TRISAbits.TRISA4 = 1; } while(0)
#define SERVICE01_DBG_SetDigitalOutput()   do { TRISAbits.TRISA4 = 0; } while(0)

// get/set SW_CHIAVE aliases
#define SW_CHIAVE_TRIS                 TRISAbits.TRISA5
#define SW_CHIAVE_LAT                  LATAbits.LATA5
#define SW_CHIAVE_PORT                 PORTAbits.RA5
#define SW_CHIAVE_ANS                  ANSELAbits.ANSA5
#define SW_CHIAVE_SetHigh()            do { LATAbits.LATA5 = 1; } while(0)
#define SW_CHIAVE_SetLow()             do { LATAbits.LATA5 = 0; } while(0)
#define SW_CHIAVE_Toggle()             do { LATAbits.LATA5 = ~LATAbits.LATA5; } while(0)
#define SW_CHIAVE_GetValue()           PORTAbits.RA5
#define SW_CHIAVE_SetDigitalInput()    do { TRISAbits.TRISA5 = 1; } while(0)
#define SW_CHIAVE_SetDigitalOutput()   do { TRISAbits.TRISA5 = 0; } while(0)
#define SW_CHIAVE_SetAnalogMode()      do { ANSELAbits.ANSA5 = 1; } while(0)
#define SW_CHIAVE_SetDigitalMode()     do { ANSELAbits.ANSA5 = 0; } while(0)

// get/set COL01 aliases
#define COL01_TRIS                 TRISBbits.TRISB0
#define COL01_LAT                  LATBbits.LATB0
#define COL01_PORT                 PORTBbits.RB0
#define COL01_WPU                  WPUBbits.WPUB0
#define COL01_ANS                  ANSELBbits.ANSB0
#define COL01_SetHigh()            do { LATBbits.LATB0 = 1; } while(0)
#define COL01_SetLow()             do { LATBbits.LATB0 = 0; } while(0)
#define COL01_Toggle()             do { LATBbits.LATB0 = ~LATBbits.LATB0; } while(0)
#define COL01_GetValue()           PORTBbits.RB0
#define COL01_SetDigitalInput()    do { TRISBbits.TRISB0 = 1; } while(0)
#define COL01_SetDigitalOutput()   do { TRISBbits.TRISB0 = 0; } while(0)
#define COL01_SetPullup()          do { WPUBbits.WPUB0 = 1; } while(0)
#define COL01_ResetPullup()        do { WPUBbits.WPUB0 = 0; } while(0)
#define COL01_SetAnalogMode()      do { ANSELBbits.ANSB0 = 1; } while(0)
#define COL01_SetDigitalMode()     do { ANSELBbits.ANSB0 = 0; } while(0)

// get/set COL00 aliases
#define COL00_TRIS                 TRISBbits.TRISB1
#define COL00_LAT                  LATBbits.LATB1
#define COL00_PORT                 PORTBbits.RB1
#define COL00_WPU                  WPUBbits.WPUB1
#define COL00_ANS                  ANSELBbits.ANSB1
#define COL00_SetHigh()            do { LATBbits.LATB1 = 1; } while(0)
#define COL00_SetLow()             do { LATBbits.LATB1 = 0; } while(0)
#define COL00_Toggle()             do { LATBbits.LATB1 = ~LATBbits.LATB1; } while(0)
#define COL00_GetValue()           PORTBbits.RB1
#define COL00_SetDigitalInput()    do { TRISBbits.TRISB1 = 1; } while(0)
#define COL00_SetDigitalOutput()   do { TRISBbits.TRISB1 = 0; } while(0)
#define COL00_SetPullup()          do { WPUBbits.WPUB1 = 1; } while(0)
#define COL00_ResetPullup()        do { WPUBbits.WPUB1 = 0; } while(0)
#define COL00_SetAnalogMode()      do { ANSELBbits.ANSB1 = 1; } while(0)
#define COL00_SetDigitalMode()     do { ANSELBbits.ANSB1 = 0; } while(0)

// get/set COL02 aliases
#define COL02_TRIS                 TRISBbits.TRISB2
#define COL02_LAT                  LATBbits.LATB2
#define COL02_PORT                 PORTBbits.RB2
#define COL02_WPU                  WPUBbits.WPUB2
#define COL02_ANS                  ANSELBbits.ANSB2
#define COL02_SetHigh()            do { LATBbits.LATB2 = 1; } while(0)
#define COL02_SetLow()             do { LATBbits.LATB2 = 0; } while(0)
#define COL02_Toggle()             do { LATBbits.LATB2 = ~LATBbits.LATB2; } while(0)
#define COL02_GetValue()           PORTBbits.RB2
#define COL02_SetDigitalInput()    do { TRISBbits.TRISB2 = 1; } while(0)
#define COL02_SetDigitalOutput()   do { TRISBbits.TRISB2 = 0; } while(0)
#define COL02_SetPullup()          do { WPUBbits.WPUB2 = 1; } while(0)
#define COL02_ResetPullup()        do { WPUBbits.WPUB2 = 0; } while(0)
#define COL02_SetAnalogMode()      do { ANSELBbits.ANSB2 = 1; } while(0)
#define COL02_SetDigitalMode()     do { ANSELBbits.ANSB2 = 0; } while(0)

// get/set COL03 aliases
#define COL03_TRIS                 TRISBbits.TRISB3
#define COL03_LAT                  LATBbits.LATB3
#define COL03_PORT                 PORTBbits.RB3
#define COL03_WPU                  WPUBbits.WPUB3
#define COL03_ANS                  ANSELBbits.ANSB3
#define COL03_SetHigh()            do { LATBbits.LATB3 = 1; } while(0)
#define COL03_SetLow()             do { LATBbits.LATB3 = 0; } while(0)
#define COL03_Toggle()             do { LATBbits.LATB3 = ~LATBbits.LATB3; } while(0)
#define COL03_GetValue()           PORTBbits.RB3
#define COL03_SetDigitalInput()    do { TRISBbits.TRISB3 = 1; } while(0)
#define COL03_SetDigitalOutput()   do { TRISBbits.TRISB3 = 0; } while(0)
#define COL03_SetPullup()          do { WPUBbits.WPUB3 = 1; } while(0)
#define COL03_ResetPullup()        do { WPUBbits.WPUB3 = 0; } while(0)
#define COL03_SetAnalogMode()      do { ANSELBbits.ANSB3 = 1; } while(0)
#define COL03_SetDigitalMode()     do { ANSELBbits.ANSB3 = 0; } while(0)

// get/set COL04 aliases
#define COL04_TRIS                 TRISBbits.TRISB4
#define COL04_LAT                  LATBbits.LATB4
#define COL04_PORT                 PORTBbits.RB4
#define COL04_WPU                  WPUBbits.WPUB4
#define COL04_ANS                  ANSELBbits.ANSB4
#define COL04_SetHigh()            do { LATBbits.LATB4 = 1; } while(0)
#define COL04_SetLow()             do { LATBbits.LATB4 = 0; } while(0)
#define COL04_Toggle()             do { LATBbits.LATB4 = ~LATBbits.LATB4; } while(0)
#define COL04_GetValue()           PORTBbits.RB4
#define COL04_SetDigitalInput()    do { TRISBbits.TRISB4 = 1; } while(0)
#define COL04_SetDigitalOutput()   do { TRISBbits.TRISB4 = 0; } while(0)
#define COL04_SetPullup()          do { WPUBbits.WPUB4 = 1; } while(0)
#define COL04_ResetPullup()        do { WPUBbits.WPUB4 = 0; } while(0)
#define COL04_SetAnalogMode()      do { ANSELBbits.ANSB4 = 1; } while(0)
#define COL04_SetDigitalMode()     do { ANSELBbits.ANSB4 = 0; } while(0)

// get/set COL05 aliases
#define COL05_TRIS                 TRISBbits.TRISB5
#define COL05_LAT                  LATBbits.LATB5
#define COL05_PORT                 PORTBbits.RB5
#define COL05_WPU                  WPUBbits.WPUB5
#define COL05_ANS                  ANSELBbits.ANSB5
#define COL05_SetHigh()            do { LATBbits.LATB5 = 1; } while(0)
#define COL05_SetLow()             do { LATBbits.LATB5 = 0; } while(0)
#define COL05_Toggle()             do { LATBbits.LATB5 = ~LATBbits.LATB5; } while(0)
#define COL05_GetValue()           PORTBbits.RB5
#define COL05_SetDigitalInput()    do { TRISBbits.TRISB5 = 1; } while(0)
#define COL05_SetDigitalOutput()   do { TRISBbits.TRISB5 = 0; } while(0)
#define COL05_SetPullup()          do { WPUBbits.WPUB5 = 1; } while(0)
#define COL05_ResetPullup()        do { WPUBbits.WPUB5 = 0; } while(0)
#define COL05_SetAnalogMode()      do { ANSELBbits.ANSB5 = 1; } while(0)
#define COL05_SetDigitalMode()     do { ANSELBbits.ANSB5 = 0; } while(0)

// get/set COL06 aliases
#define COL06_TRIS                 TRISBbits.TRISB6
#define COL06_LAT                  LATBbits.LATB6
#define COL06_PORT                 PORTBbits.RB6
#define COL06_WPU                  WPUBbits.WPUB6
#define COL06_SetHigh()            do { LATBbits.LATB6 = 1; } while(0)
#define COL06_SetLow()             do { LATBbits.LATB6 = 0; } while(0)
#define COL06_Toggle()             do { LATBbits.LATB6 = ~LATBbits.LATB6; } while(0)
#define COL06_GetValue()           PORTBbits.RB6
#define COL06_SetDigitalInput()    do { TRISBbits.TRISB6 = 1; } while(0)
#define COL06_SetDigitalOutput()   do { TRISBbits.TRISB6 = 0; } while(0)
#define COL06_SetPullup()          do { WPUBbits.WPUB6 = 1; } while(0)
#define COL06_ResetPullup()        do { WPUBbits.WPUB6 = 0; } while(0)

// get/set COL07 aliases
#define COL07_TRIS                 TRISBbits.TRISB7
#define COL07_LAT                  LATBbits.LATB7
#define COL07_PORT                 PORTBbits.RB7
#define COL07_WPU                  WPUBbits.WPUB7
#define COL07_SetHigh()            do { LATBbits.LATB7 = 1; } while(0)
#define COL07_SetLow()             do { LATBbits.LATB7 = 0; } while(0)
#define COL07_Toggle()             do { LATBbits.LATB7 = ~LATBbits.LATB7; } while(0)
#define COL07_GetValue()           PORTBbits.RB7
#define COL07_SetDigitalInput()    do { TRISBbits.TRISB7 = 1; } while(0)
#define COL07_SetDigitalOutput()   do { TRISBbits.TRISB7 = 0; } while(0)
#define COL07_SetPullup()          do { WPUBbits.WPUB7 = 1; } while(0)
#define COL07_ResetPullup()        do { WPUBbits.WPUB7 = 0; } while(0)

// get/set COL09 aliases
#define COL09_TRIS                 TRISCbits.TRISC0
#define COL09_LAT                  LATCbits.LATC0
#define COL09_PORT                 PORTCbits.RC0
#define COL09_SetHigh()            do { LATCbits.LATC0 = 1; } while(0)
#define COL09_SetLow()             do { LATCbits.LATC0 = 0; } while(0)
#define COL09_Toggle()             do { LATCbits.LATC0 = ~LATCbits.LATC0; } while(0)
#define COL09_GetValue()           PORTCbits.RC0
#define COL09_SetDigitalInput()    do { TRISCbits.TRISC0 = 1; } while(0)
#define COL09_SetDigitalOutput()   do { TRISCbits.TRISC0 = 0; } while(0)

// get/set COL08 aliases
#define COL08_TRIS                 TRISCbits.TRISC1
#define COL08_LAT                  LATCbits.LATC1
#define COL08_PORT                 PORTCbits.RC1
#define COL08_SetHigh()            do { LATCbits.LATC1 = 1; } while(0)
#define COL08_SetLow()             do { LATCbits.LATC1 = 0; } while(0)
#define COL08_Toggle()             do { LATCbits.LATC1 = ~LATCbits.LATC1; } while(0)
#define COL08_GetValue()           PORTCbits.RC1
#define COL08_SetDigitalInput()    do { TRISCbits.TRISC1 = 1; } while(0)
#define COL08_SetDigitalOutput()   do { TRISCbits.TRISC1 = 0; } while(0)

// get/set RC2 procedures
#define RC2_SetHigh()            do { LATCbits.LATC2 = 1; } while(0)
#define RC2_SetLow()             do { LATCbits.LATC2 = 0; } while(0)
#define RC2_Toggle()             do { LATCbits.LATC2 = ~LATCbits.LATC2; } while(0)
#define RC2_GetValue()              PORTCbits.RC2
#define RC2_SetDigitalInput()    do { TRISCbits.TRISC2 = 1; } while(0)
#define RC2_SetDigitalOutput()   do { TRISCbits.TRISC2 = 0; } while(0)
#define RC2_SetAnalogMode()         do { ANSELCbits.ANSC2 = 1; } while(0)
#define RC2_SetDigitalMode()        do { ANSELCbits.ANSC2 = 0; } while(0)

// get/set RC6 procedures
#define RC6_SetHigh()            do { LATCbits.LATC6 = 1; } while(0)
#define RC6_SetLow()             do { LATCbits.LATC6 = 0; } while(0)
#define RC6_Toggle()             do { LATCbits.LATC6 = ~LATCbits.LATC6; } while(0)
#define RC6_GetValue()              PORTCbits.RC6
#define RC6_SetDigitalInput()    do { TRISCbits.TRISC6 = 1; } while(0)
#define RC6_SetDigitalOutput()   do { TRISCbits.TRISC6 = 0; } while(0)
#define RC6_SetAnalogMode()         do { ANSELCbits.ANSC6 = 1; } while(0)
#define RC6_SetDigitalMode()        do { ANSELCbits.ANSC6 = 0; } while(0)

// get/set RC7 procedures
#define RC7_SetHigh()            do { LATCbits.LATC7 = 1; } while(0)
#define RC7_SetLow()             do { LATCbits.LATC7 = 0; } while(0)
#define RC7_Toggle()             do { LATCbits.LATC7 = ~LATCbits.LATC7; } while(0)
#define RC7_GetValue()              PORTCbits.RC7
#define RC7_SetDigitalInput()    do { TRISCbits.TRISC7 = 1; } while(0)
#define RC7_SetDigitalOutput()   do { TRISCbits.TRISC7 = 0; } while(0)
#define RC7_SetAnalogMode()         do { ANSELCbits.ANSC7 = 1; } while(0)
#define RC7_SetDigitalMode()        do { ANSELCbits.ANSC7 = 0; } while(0)

// get/set ROW00 aliases
#define ROW00_TRIS                 TRISDbits.TRISD0
#define ROW00_LAT                  LATDbits.LATD0
#define ROW00_PORT                 PORTDbits.RD0
#define ROW00_ANS                  ANSELDbits.ANSD0
#define ROW00_SetHigh()            do { LATDbits.LATD0 = 1; } while(0)
#define ROW00_SetLow()             do { LATDbits.LATD0 = 0; } while(0)
#define ROW00_Toggle()             do { LATDbits.LATD0 = ~LATDbits.LATD0; } while(0)
#define ROW00_GetValue()           PORTDbits.RD0
#define ROW00_SetDigitalInput()    do { TRISDbits.TRISD0 = 1; } while(0)
#define ROW00_SetDigitalOutput()   do { TRISDbits.TRISD0 = 0; } while(0)
#define ROW00_SetAnalogMode()      do { ANSELDbits.ANSD0 = 1; } while(0)
#define ROW00_SetDigitalMode()     do { ANSELDbits.ANSD0 = 0; } while(0)

// get/set ROW01 aliases
#define ROW01_TRIS                 TRISDbits.TRISD1
#define ROW01_LAT                  LATDbits.LATD1
#define ROW01_PORT                 PORTDbits.RD1
#define ROW01_ANS                  ANSELDbits.ANSD1
#define ROW01_SetHigh()            do { LATDbits.LATD1 = 1; } while(0)
#define ROW01_SetLow()             do { LATDbits.LATD1 = 0; } while(0)
#define ROW01_Toggle()             do { LATDbits.LATD1 = ~LATDbits.LATD1; } while(0)
#define ROW01_GetValue()           PORTDbits.RD1
#define ROW01_SetDigitalInput()    do { TRISDbits.TRISD1 = 1; } while(0)
#define ROW01_SetDigitalOutput()   do { TRISDbits.TRISD1 = 0; } while(0)
#define ROW01_SetAnalogMode()      do { ANSELDbits.ANSD1 = 1; } while(0)
#define ROW01_SetDigitalMode()     do { ANSELDbits.ANSD1 = 0; } while(0)

// get/set ROW02 aliases
#define ROW02_TRIS                 TRISDbits.TRISD2
#define ROW02_LAT                  LATDbits.LATD2
#define ROW02_PORT                 PORTDbits.RD2
#define ROW02_ANS                  ANSELDbits.ANSD2
#define ROW02_SetHigh()            do { LATDbits.LATD2 = 1; } while(0)
#define ROW02_SetLow()             do { LATDbits.LATD2 = 0; } while(0)
#define ROW02_Toggle()             do { LATDbits.LATD2 = ~LATDbits.LATD2; } while(0)
#define ROW02_GetValue()           PORTDbits.RD2
#define ROW02_SetDigitalInput()    do { TRISDbits.TRISD2 = 1; } while(0)
#define ROW02_SetDigitalOutput()   do { TRISDbits.TRISD2 = 0; } while(0)
#define ROW02_SetAnalogMode()      do { ANSELDbits.ANSD2 = 1; } while(0)
#define ROW02_SetDigitalMode()     do { ANSELDbits.ANSD2 = 0; } while(0)

// get/set ROW03 aliases
#define ROW03_TRIS                 TRISDbits.TRISD3
#define ROW03_LAT                  LATDbits.LATD3
#define ROW03_PORT                 PORTDbits.RD3
#define ROW03_ANS                  ANSELDbits.ANSD3
#define ROW03_SetHigh()            do { LATDbits.LATD3 = 1; } while(0)
#define ROW03_SetLow()             do { LATDbits.LATD3 = 0; } while(0)
#define ROW03_Toggle()             do { LATDbits.LATD3 = ~LATDbits.LATD3; } while(0)
#define ROW03_GetValue()           PORTDbits.RD3
#define ROW03_SetDigitalInput()    do { TRISDbits.TRISD3 = 1; } while(0)
#define ROW03_SetDigitalOutput()   do { TRISDbits.TRISD3 = 0; } while(0)
#define ROW03_SetAnalogMode()      do { ANSELDbits.ANSD3 = 1; } while(0)
#define ROW03_SetDigitalMode()     do { ANSELDbits.ANSD3 = 0; } while(0)

// get/set ROW04 aliases
#define ROW04_TRIS                 TRISDbits.TRISD4
#define ROW04_LAT                  LATDbits.LATD4
#define ROW04_PORT                 PORTDbits.RD4
#define ROW04_ANS                  ANSELDbits.ANSD4
#define ROW04_SetHigh()            do { LATDbits.LATD4 = 1; } while(0)
#define ROW04_SetLow()             do { LATDbits.LATD4 = 0; } while(0)
#define ROW04_Toggle()             do { LATDbits.LATD4 = ~LATDbits.LATD4; } while(0)
#define ROW04_GetValue()           PORTDbits.RD4
#define ROW04_SetDigitalInput()    do { TRISDbits.TRISD4 = 1; } while(0)
#define ROW04_SetDigitalOutput()   do { TRISDbits.TRISD4 = 0; } while(0)
#define ROW04_SetAnalogMode()      do { ANSELDbits.ANSD4 = 1; } while(0)
#define ROW04_SetDigitalMode()     do { ANSELDbits.ANSD4 = 0; } while(0)

// get/set ROW05 aliases
#define ROW05_TRIS                 TRISDbits.TRISD5
#define ROW05_LAT                  LATDbits.LATD5
#define ROW05_PORT                 PORTDbits.RD5
#define ROW05_ANS                  ANSELDbits.ANSD5
#define ROW05_SetHigh()            do { LATDbits.LATD5 = 1; } while(0)
#define ROW05_SetLow()             do { LATDbits.LATD5 = 0; } while(0)
#define ROW05_Toggle()             do { LATDbits.LATD5 = ~LATDbits.LATD5; } while(0)
#define ROW05_GetValue()           PORTDbits.RD5
#define ROW05_SetDigitalInput()    do { TRISDbits.TRISD5 = 1; } while(0)
#define ROW05_SetDigitalOutput()   do { TRISDbits.TRISD5 = 0; } while(0)
#define ROW05_SetAnalogMode()      do { ANSELDbits.ANSD5 = 1; } while(0)
#define ROW05_SetDigitalMode()     do { ANSELDbits.ANSD5 = 0; } while(0)

// get/set ROW06 aliases
#define ROW06_TRIS                 TRISDbits.TRISD6
#define ROW06_LAT                  LATDbits.LATD6
#define ROW06_PORT                 PORTDbits.RD6
#define ROW06_ANS                  ANSELDbits.ANSD6
#define ROW06_SetHigh()            do { LATDbits.LATD6 = 1; } while(0)
#define ROW06_SetLow()             do { LATDbits.LATD6 = 0; } while(0)
#define ROW06_Toggle()             do { LATDbits.LATD6 = ~LATDbits.LATD6; } while(0)
#define ROW06_GetValue()           PORTDbits.RD6
#define ROW06_SetDigitalInput()    do { TRISDbits.TRISD6 = 1; } while(0)
#define ROW06_SetDigitalOutput()   do { TRISDbits.TRISD6 = 0; } while(0)
#define ROW06_SetAnalogMode()      do { ANSELDbits.ANSD6 = 1; } while(0)
#define ROW06_SetDigitalMode()     do { ANSELDbits.ANSD6 = 0; } while(0)

// get/set ROW07 aliases
#define ROW07_TRIS                 TRISDbits.TRISD7
#define ROW07_LAT                  LATDbits.LATD7
#define ROW07_PORT                 PORTDbits.RD7
#define ROW07_ANS                  ANSELDbits.ANSD7
#define ROW07_SetHigh()            do { LATDbits.LATD7 = 1; } while(0)
#define ROW07_SetLow()             do { LATDbits.LATD7 = 0; } while(0)
#define ROW07_Toggle()             do { LATDbits.LATD7 = ~LATDbits.LATD7; } while(0)
#define ROW07_GetValue()           PORTDbits.RD7
#define ROW07_SetDigitalInput()    do { TRISDbits.TRISD7 = 1; } while(0)
#define ROW07_SetDigitalOutput()   do { TRISDbits.TRISD7 = 0; } while(0)
#define ROW07_SetAnalogMode()      do { ANSELDbits.ANSD7 = 1; } while(0)
#define ROW07_SetDigitalMode()     do { ANSELDbits.ANSD7 = 0; } while(0)

// get/set ROW10 aliases
#define ROW10_TRIS                 TRISEbits.TRISE0
#define ROW10_LAT                  LATEbits.LATE0
#define ROW10_PORT                 PORTEbits.RE0
#define ROW10_ANS                  ANSELEbits.ANSE0
#define ROW10_SetHigh()            do { LATEbits.LATE0 = 1; } while(0)
#define ROW10_SetLow()             do { LATEbits.LATE0 = 0; } while(0)
#define ROW10_Toggle()             do { LATEbits.LATE0 = ~LATEbits.LATE0; } while(0)
#define ROW10_GetValue()           PORTEbits.RE0
#define ROW10_SetDigitalInput()    do { TRISEbits.TRISE0 = 1; } while(0)
#define ROW10_SetDigitalOutput()   do { TRISEbits.TRISE0 = 0; } while(0)
#define ROW10_SetAnalogMode()      do { ANSELEbits.ANSE0 = 1; } while(0)
#define ROW10_SetDigitalMode()     do { ANSELEbits.ANSE0 = 0; } while(0)

// get/set ROW09 aliases
#define ROW09_TRIS                 TRISEbits.TRISE1
#define ROW09_LAT                  LATEbits.LATE1
#define ROW09_PORT                 PORTEbits.RE1
#define ROW09_ANS                  ANSELEbits.ANSE1
#define ROW09_SetHigh()            do { LATEbits.LATE1 = 1; } while(0)
#define ROW09_SetLow()             do { LATEbits.LATE1 = 0; } while(0)
#define ROW09_Toggle()             do { LATEbits.LATE1 = ~LATEbits.LATE1; } while(0)
#define ROW09_GetValue()           PORTEbits.RE1
#define ROW09_SetDigitalInput()    do { TRISEbits.TRISE1 = 1; } while(0)
#define ROW09_SetDigitalOutput()   do { TRISEbits.TRISE1 = 0; } while(0)
#define ROW09_SetAnalogMode()      do { ANSELEbits.ANSE1 = 1; } while(0)
#define ROW09_SetDigitalMode()     do { ANSELEbits.ANSE1 = 0; } while(0)

// get/set ROW08 aliases
#define ROW08_TRIS                 TRISEbits.TRISE2
#define ROW08_LAT                  LATEbits.LATE2
#define ROW08_PORT                 PORTEbits.RE2
#define ROW08_ANS                  ANSELEbits.ANSE2
#define ROW08_SetHigh()            do { LATEbits.LATE2 = 1; } while(0)
#define ROW08_SetLow()             do { LATEbits.LATE2 = 0; } while(0)
#define ROW08_Toggle()             do { LATEbits.LATE2 = ~LATEbits.LATE2; } while(0)
#define ROW08_GetValue()           PORTEbits.RE2
#define ROW08_SetDigitalInput()    do { TRISEbits.TRISE2 = 1; } while(0)
#define ROW08_SetDigitalOutput()   do { TRISEbits.TRISE2 = 0; } while(0)
#define ROW08_SetAnalogMode()      do { ANSELEbits.ANSE2 = 1; } while(0)
#define ROW08_SetDigitalMode()     do { ANSELEbits.ANSE2 = 0; } while(0)

/**
   @Param
    none
   @Returns
    none
   @Description
    GPIO and peripheral I/O initialization
   @Example
    PIN_MANAGER_Initialize();
 */
void PIN_MANAGER_Initialize (void);

/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Interrupt on Change Handling routine
 * @Example
    PIN_MANAGER_IOC();
 */
void PIN_MANAGER_IOC(void);



#endif // PIN_MANAGER_H
/**
 End of File
*/