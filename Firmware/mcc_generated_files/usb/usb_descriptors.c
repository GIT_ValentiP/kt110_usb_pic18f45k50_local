


/*******************************************************************************
Copyright 2016 Microchip Technology Inc. (www.microchip.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

To request to license the code under the MLA license (www.microchip.com/mla_license), 
please contact mla_licensing@microchip.com
*******************************************************************************/

/********************************************************************
-usb_descriptors.c-
-------------------------------------------------------------------
Filling in the descriptor values in the usb_descriptors.c file:
-------------------------------------------------------------------

[Device Descriptors]
The device descriptor is defined as a USB_DEVICE_DESCRIPTOR type.  
This type is defined in usb_ch9.h  Each entry into this structure
needs to be the correct length for the data type of the entry.

[Configuration Descriptors]
The configuration descriptor was changed in v2.x from a structure
to a uint8_t array.  Given that the configuration is now a byte array
each byte of multi-byte fields must be listed individually.  This
means that for fields like the total size of the configuration where
the field is a 16-bit value "64,0," is the correct entry for a
configuration that is only 64 bytes long and not "64," which is one
too few bytes.

The configuration attribute must always have the _DEFAULT
definition at the minimum. Additional options can be ORed
to the _DEFAULT attribute. Available options are _SELF and _RWU.
These definitions are defined in the usb_device.h file. The
_SELF tells the USB host that this device is self-powered. The
_RWU tells the USB host that this device supports Remote Wakeup.

[Endpoint Descriptors]
Like the configuration descriptor, the endpoint descriptors were 
changed in v2.x of the stack from a structure to a uint8_t array.  As
endpoint descriptors also has a field that are multi-byte entities,
please be sure to specify both bytes of the field.  For example, for
the endpoint size an endpoint that is 64 bytes needs to have the size
defined as "64,0," instead of "64,"

Take the following example:
    // Endpoint Descriptor //
    0x07,                       //the size of this descriptor //
    USB_DESCRIPTOR_ENDPOINT,    //Endpoint Descriptor
    _EP02_IN,                   //EndpointAddress
    _INT,                       //Attributes
    0x08,0x00,                  //size (note: 2 bytes)
    0x02,                       //Interval

The first two parameters are self-explanatory. They specify the
length of this endpoint descriptor (7) and the descriptor type.
The next parameter identifies the endpoint, the definitions are
defined in usb_device.h and has the following naming
convention:
_EP<##>_<dir>
where ## is the endpoint number and dir is the direction of
transfer. The dir has the value of either 'OUT' or 'IN'.
The next parameter identifies the type of the endpoint. Available
options are _BULK, _INT, _ISO, and _CTRL. The _CTRL is not
typically used because the default control transfer endpoint is
not defined in the USB descriptors. When _ISO option is used,
addition options can be ORed to _ISO. Example:
_ISO|_AD|_FE
This describes the endpoint as an isochronous pipe with adaptive
and feedback attributes. See usb_device.h and the USB
specification for details. The next parameter defines the size of
the endpoint. The last parameter in the polling interval.

-------------------------------------------------------------------
Adding a USB String
-------------------------------------------------------------------
A string descriptor array should have the following format:

rom struct{byte bLength;byte bDscType;word string[size];}sdxxx={
sizeof(sdxxx),DSC_STR,<text>};

The above structure provides a means for the C compiler to
calculate the length of string descriptor sdxxx, where xxx is the
index number. The first two bytes of the descriptor are descriptor
length and type. The rest <text> are string texts which must be
in the unicode format. The unicode format is achieved by declaring
each character as a word type. The whole text string is declared
as a word array with the number of characters equals to <size>.
<size> has to be manually counted and entered into the array
declaration. Let's study this through an example:
if the string is "USB" , then the string descriptor should be:
(Using index 02)
rom struct{byte bLength;byte bDscType;word string[3];}sd002={
sizeof(sd002),DSC_STR,'U','S','B'};

A USB project may have multiple strings and the firmware supports
the management of multiple strings through a look-up table.
The look-up table is defined as:
rom const unsigned char *rom USB_SD_Ptr[]={&sd000,&sd001,&sd002};

The above declaration has 3 strings, sd000, sd001, and sd002.
Strings can be removed or added. sd000 is a specialized string
descriptor. It defines the language code, usually this is
US English (0x0409). The index of the string must match the index
position of the USB_SD_Ptr array, &sd000 must be in position
USB_SD_Ptr[0], &sd001 must be in position USB_SD_Ptr[1] and so on.
The look-up table USB_SD_Ptr is used by the get string handler
function.

-------------------------------------------------------------------

The look-up table scheme also applies to the configuration
descriptor. A USB device may have multiple configuration
descriptors, i.e. CFG01, CFG02, etc. To add a configuration
descriptor, user must implement a structure similar to CFG01.
The next step is to add the configuration descriptor name, i.e.
cfg01, cfg02,.., to the look-up table USB_CD_Ptr. USB_CD_Ptr[0]
is a dummy place holder since configuration 0 is the un-configured
state according to the definition in the USB specification.

********************************************************************/
 
/*********************************************************************
 * Descriptor specific type definitions are defined in:
 * usb_device.h
 *
 * Configuration options are defined in:
 * usb_device_config.h
 ********************************************************************/
#ifndef __USB_DESCRIPTORS_C
#define __USB_DESCRIPTORS_C
 
/** INCLUDES *******************************************************/
#include "usb.h"
#include "usb_device_generic.h"
#include "usb_device_hid.h"
#include "../../globals.h"
/** CONSTANTS ******************************************************/
#if defined(__18CXX)
#pragma romdata
#endif

#ifdef USB_USE_HID_KEYBOARD_MULTIMEDIA

//Class specific descriptor - HID Keyboard
const struct{uint8_t report[HID_RPT01_SIZE];} hid_rpt01={   
  {
      
   0x05, 0x01, /* Usage Page (Generic Desktop)             */
   0x09, 0x06, /* Usage (Keyboard)                         */
   0xA1, 0x01, /* Collection (Application)                 */
   0x05, 0x07, /*  Usage (Key codes)                       */
   0x19, 0xE0, /*      Usage Minimum (224)                  */
   0x29, 0xE7, /*      Usage Maximum (231)                  */
   0x15, 0x00, /*      Logical Minimum (0)                 */
   0x25, 0x01, /*      Logical Maximum (1)                 */
   0x75, 0x01, /*      Report Size (1)                     */
   0x95, 0x08, /*      Report Count (8)                    */
   0x81, 0x02, /*      input (data, variable, absolute)    */
   0x95, 0x01, /*      Report Count (1)                     */
   0x75, 0x08, /*      Report Size (8)                    */
   0x81, 0x01, /*      input constant    */
   0x95, 0x05, /*      Report Count (3)                     */
   0x75, 0x01, /*      Report Size (1)                    */
   0x05, 0x08, /*      Usage Page (Leds)                    */
   0x19, 0x01, /*      Usage Minimum (1)                  */
   0x29, 0x05, /*      Usage Maximum (5)                  */
   0x91, 0x02, /*      Output (data, variable, absolute)  */
   0x95, 0x01, /*      Report Count (1)                     */
   0x75, 0x03, /*      Report Size (3)                    */
   0x91, 0x01, /*      output constant    */
   0x95, 0x06, /*      Report Count (6)                     */
   0x75, 0x08, /*      Report Size (8)                    */
   0x15, 0x00, /*      Logical Minimum (0)                 */
   0x25, 0x65, /*      Logical Maximum (101)                 */
   0x05, 0x07, /*  Usage (Key codes)                       */
   0x19, 0x00, /*      Usage Minimum (0)                 */
   0x29, 0x65, /*      Usage Maximum (101)                 */
   0x81, 0x00, /*      Input (data, array)  */
   0xC0}	/* End Collection            */
};
const struct{uint8_t report[HID_RPT02_SIZE];} hid_rpt02={ 
   {0x06, 0x0C, 0x00, // usage page (consumer)
	0x09, 0x01, // usage (consumer control)
	0xA1, 0x01, // collection (application)
	0x85, 0x01, // ReportID 1
	0x25, 0x01, // logical maximum
	0x15, 0x00, // logical minimum
	0x75, 0x01, // Report Size 
	0x0A, 0x83, 0x01, // usage 
	0x0A, 0xCD, 0x00, // usage 
	0x0A, 0xE2, 0x00, // usage 
	0x0A, 0xE9, 0x00, // usage 
	0x0A, 0xEA, 0x00, // usage 
	0x0A, 0x8A, 0x01, // usage 
	0x0A, 0x23, 0x02, // usage 
	0x0A, 0x92, 0x01, // usage 
	0x95, 0x08, // Report Count
	0x81, 0x02, // input (variable)
	0xC0}  // end collection
};    

/* Device Descriptor */
const USB_DEVICE_DESCRIPTOR device_dsc=
{
    0x12,    // Size of this descriptor in bytes
    USB_DESCRIPTOR_DEVICE,                // DEVICE descriptor type
    0x0200,                 // USB Spec Release Number in BCD format
    0x00,                   // Class Code
    0x00,                   // Subclass code
    0x00,                   // Protocol code
    USB_EP0_BUFF_SIZE,      // Max packet size for EP0, see usb_config.h
    MY_VID,                 // Vendor ID
    MY_PID,                 // Product ID: Keyboard fw demo
    0x0226,// 31/07/2020    // Device release number in BCD format  old 0x0220
    0x01,                   // Manufacturer string index
    0x02,                   // Product string index
    0x00,                   // Device serial number string index
    0x01                    // Number of possible configurations
};


/* Configuration 1 Descriptor */
const uint8_t configDescriptor1[]={ 
  /* Configuration Descriptor */
    0x09,//sizeof(USB_CFG_DSC),  // Size of this descriptor in bytes
    USB_DESCRIPTOR_CONFIGURATION,// CONFIGURATION descriptor type
    DESC_CONFIG_WORD(0x003B),    // Total length of data for this cfg
    2,                           // Number of interfaces in this cfg
    1,                           // Index value of this configuration
    3,                           // Configuration string index
    _DEFAULT | _RWU,             // Attributes, see usb_device.h
    50,                          // Max power consumption (2X mA)

    /* Interface Descriptor1 */
    0x09,//sizeof(USB_INTF_DSC),   // Size of this descriptor in bytes
    USB_DESCRIPTOR_INTERFACE,               // INTERFACE descriptor type
    0,                      // Interface Number
    0,                      // Alternate Setting Number
    1,                      // Number of endpoints in this intf
    HID_INTF,               // Class code
    BOOT_INTF_SUBCLASS,     // Subclass code
    HID_PROTOCOL_KEYBOARD,  // Protocol code
    5,                      // Interface string index

    /* HID Class-Specific Descriptor #1*/
    0x09,//sizeof(USB_HID_DSC),   // Size of this descriptor in bytes RRoj hack
    DSC_HID,                      // HID descriptor type
    DESC_CONFIG_WORD(0x0111),     // HID Spec Release Number in BCD format (1.11)
    0x0D,                         // Country Code (0x00 for Not supported)
    HID_NUM_OF_DSC,               // Number of class descriptors, see usbcfg.h
    DSC_RPT,                      // Report descriptor type
    DESC_CONFIG_WORD(63),         //sizeof(hid_rpt01),      // Size of the report descriptor
    
    /* Endpoint Descriptor */
    0x07,                       //sizeof(USB_EP_DSC)
    USB_DESCRIPTOR_ENDPOINT,    //Endpoint Descriptor
    HID_EP1 | _EP_IN,           //EndpointAddress
    _INTERRUPT,                 //Attributes
    DESC_CONFIG_WORD(HID_INT_IN_EP1_SIZE),        //size
    0x0A,                       //Interval

    /* Interface Descriptor2 */
    0x09,//sizeof(USB_INTF_DSC),// Size of this descriptor in bytes
    USB_DESCRIPTOR_INTERFACE,   // INTERFACE descriptor type
    1,                          // Interface Number
    0,                          // Alternate Setting Number
    1,                          // Number of endpoints in this intf
    HID_INTF,                   // Class code
    BOOT_INTF_SUBCLASS,         // Subclass code
    HID_PROTOCOL_NONE,          // Protocol codeHID_PROTOCOL_KEYBOARD,
    6,                          // Interface string index
    
    /* HID Class-Specific Descriptor #2*/
    0x09,//sizeof(USB_HID_DSC)+3,    // Size of this descriptor in bytes RRoj hack
    DSC_HID,                         // HID descriptor type
    DESC_CONFIG_WORD(0x0111),        // HID Spec Release Number in BCD format (1.11)
    0x0D,                            // Country Code (0x00 for Not supported)
    HID_NUM_OF_DSC,                  // Number of class descriptors, see usbcfg.h
    DSC_RPT,                         // Report descriptor type
    DESC_CONFIG_WORD(44),            // Size of the report descriptor //sizeof(hid_rpt02),

    /* Endpoint1 Descriptor #2*/
    0x07,                         //sizeof(USB_EP_DSC)
    USB_DESCRIPTOR_ENDPOINT,      //Endpoint Descriptor
    HID_EP2 | _EP_IN,             //EndpointAddress
    _INTERRUPT,                   //Attributes
    DESC_CONFIG_WORD(HID_INT_IN_EP2_SIZE),          //size
    0x0A,                         //Interval
 };  

//Language code string descriptor
const struct{uint8_t bLength;uint8_t bDscType;uint16_t string[1];}sd000={
sizeof(sd000),USB_DESCRIPTOR_STRING,{0x0409}
};
//Manufacturer string descriptor
const struct{uint8_t bLength;uint8_t bDscType;uint16_t string[8];} sd001=  {
sizeof(sd001),USB_DESCRIPTOR_STRING,{'K','-','T','r','o','n','i','c'}
};   

const struct{uint8_t bLength;uint8_t bDscType;uint16_t string[21]; } sd002 = {
	sizeof(sd002),USB_DESCRIPTOR_STRING,{'U','S','B',' ','K','e','y','b','o','a','r','d',' ','K','-','T','r','o','n','i','c'}
};    
const struct{uint8_t bLength;uint8_t bDscType;uint16_t string[5]; } sd003 = {
	sizeof(sd003),USB_DESCRIPTOR_STRING,{'V','2','.','2','5'}
};
const struct{uint8_t bLength;uint8_t bDscType;uint16_t string[8]; } sd004 = {
	sizeof(sd004),USB_DESCRIPTOR_STRING,{'K','e','y','b','o','a','r','d'}
};
const struct{uint8_t bLength;uint8_t bDscType;uint16_t string[8]; } sd005 = {
	sizeof(sd005),USB_DESCRIPTOR_STRING,{'B','a','s','e','P','i','p','e'}
};
const struct{uint8_t bLength;uint8_t bDscType;uint16_t string[10]; } sd006 = {
	sizeof(sd005),USB_DESCRIPTOR_STRING,{'M','u','l','t','i','m','e','d','i','a'}
};
  
//Array of configuration descriptors
const uint8_t *const USB_CD_Ptr[]=
{
    (const uint8_t *const)&configDescriptor1
};

//Array of string descriptors
const uint8_t *const USB_SD_Ptr[USB_NUM_STRING_DESCRIPTORS]=
{
    (const uint8_t *const)&sd000,
    (const uint8_t *const)&sd001,
    (const uint8_t *const)&sd002,
    (const uint8_t *const)&sd003,
    (const uint8_t *const)&sd004,
    (const uint8_t *const)&sd005,
    (const uint8_t *const)&sd006,        
};
#endif


#if defined(USB_USE_HID_MOUSE) && defined(USB_USE_HID_KEYBOARD)
//Class specific descriptor - HID Keyboard
const struct{uint8_t report[HID_RPT01_SIZE];} hid_rpt01={
   {0x05, 0x01, /* Usage Page (Generic Desktop)             */
    0x09, 0x02, /* Usage (Mouse)                            */
    0xA1, 0x01, /* Collection (Application)                 */
    0x09, 0x01, /*  Usage (Pointer)                         */
    0xA1, 0x00, /*  Collection (Physical)                   */
    0x05, 0x09, /*      Usage Page (Buttons)                */
    0x19, 0x01, /*      Usage Minimum (01)                  */
    0x29, 0x03, /*      Usage Maximum (03)                  */
    0x15, 0x00, /*      Logical Minimum (0)                 */
    0x25, 0x01, /*      Logical Maximum (0)                 */
    0x95, 0x03, /*      Report Count (3)                    */
    0x75, 0x01, /*      Report Size (1)                     */
    0x81, 0x02, /*      Input (Data, Variable, Absolute)    */
    0x95, 0x01, /*      Report Count (1)                    */
    0x75, 0x05, /*      Report Size (5)                     */
    0x81, 0x01, /*      Input (Constant)    ;5 bit padding  */
    0x05, 0x01, /*      Usage Page (Generic Desktop)        */
    0x09, 0x30, /*      Usage (X)                           */
    0x09, 0x31, /*      Usage (Y)                           */
    0x15, 0x81, /*      Logical Minimum (-127)              */
    0x25, 0x7F, /*      Logical Maximum (127)               */
    0x75, 0x08, /*      Report Size (8)                     */
    0x95, 0x02, /*      Report Count (2)                    */
    0x81, 0x06, /*      Input (Data, Variable, Relative)    */
    0xC0, 0xC0}
};/* End Collection,End Collection            */


const struct{uint8_t report[HID_RPT02_SIZE];} hid_rpt02={
  {   0x05, 0x01,                    // USAGE_PAGE (Generic Desktop)
    0x09, 0x06,                    // USAGE (Keyboard)
    0xa1, 0x01,                    // COLLECTION (Application)
    0x05, 0x07,                    //   USAGE_PAGE (Keyboard)
    0x19, 0xe0,                    //   USAGE_MINIMUM (Keyboard LeftControl)
    0x29, 0xe7,                    //   USAGE_MAXIMUM (Keyboard Right GUI)
    0x15, 0x00,                    //   LOGICAL_MINIMUM (0)
    0x25, 0x01,                    //   LOGICAL_MAXIMUM (1)
    0x75, 0x01,                    //   REPORT_SIZE (1)
    0x95, 0x08,                    //   REPORT_COUNT (8)
    0x81, 0x02,                    //   INPUT (Data,Var,Abs)
    0x95, 0x01,                    //   REPORT_COUNT (1)
    0x75, 0x08,                    //   REPORT_SIZE (8)
    0x81, 0x03,                    //   INPUT (Cnst,Var,Abs)
    0x95, 0x05,                    //   REPORT_COUNT (5)
    0x75, 0x01,                    //   REPORT_SIZE (1)
    0x05, 0x08,                    //   USAGE_PAGE (LEDs)
    0x19, 0x01,                    //   USAGE_MINIMUM (Num Lock)
    0x29, 0x05,                    //   USAGE_MAXIMUM (Kana)
    0x91, 0x02,                    //   OUTPUT (Data,Var,Abs)
    0x95, 0x01,                    //   REPORT_COUNT (1)
    0x75, 0x03,                    //   REPORT_SIZE (3)
    0x91, 0x03,                    //   OUTPUT (Cnst,Var,Abs)
    0x95, 0x06,                    //   REPORT_COUNT (6)
    0x75, 0x08,                    //   REPORT_SIZE (8)
    0x15, 0x00,                    //   LOGICAL_MINIMUM (0)
    0x25, 0x65,                    //   LOGICAL_MAXIMUM (101)
    0x05, 0x07,                    //   USAGE_PAGE (Keyboard)
    0x19, 0x00,                    //   USAGE_MINIMUM (Reserved (no event indicated))
    0x29, 0x65,                    //   USAGE_MAXIMUM (Keyboard Application)
    0x81, 0x00,                    //   INPUT (Data,Ary,Abs)
    0xc0}                          // End Collection
 
};	

#else

#ifdef USB_USE_HID_KEYBOARD
//Class specific descriptor - HID Keyboard
const struct{uint8_t report[HID_RPT02_SIZE];} hid_rpt02={
{   0x05, 0x01,                    // USAGE_PAGE (Generic Desktop)
    0x09, 0x06,                    // USAGE (Keyboard)
    0xa1, 0x01,                    // COLLECTION (Application)
    0x05, 0x07,                    //   USAGE_PAGE (Keyboard)
    0x19, 0xe0,                    //   USAGE_MINIMUM (Keyboard LeftControl)
    0x29, 0xe7,                    //   USAGE_MAXIMUM (Keyboard Right GUI)
    0x15, 0x00,                    //   LOGICAL_MINIMUM (0)
    0x25, 0x01,                    //   LOGICAL_MAXIMUM (1)
    0x75, 0x01,                    //   REPORT_SIZE (1)
    0x95, 0x08,                    //   REPORT_COUNT (8)
    0x81, 0x02,                    //   INPUT (Data,Var,Abs)
    0x95, 0x01,                    //   REPORT_COUNT (1)
    0x75, 0x08,                    //   REPORT_SIZE (8)
    0x81, 0x03,                    //   INPUT (Cnst,Var,Abs)
    0x95, 0x05,                    //   REPORT_COUNT (5)
    0x75, 0x01,                    //   REPORT_SIZE (1)
    0x05, 0x08,                    //   USAGE_PAGE (LEDs)
    0x19, 0x01,                    //   USAGE_MINIMUM (Num Lock)
    0x29, 0x05,                    //   USAGE_MAXIMUM (Kana)
    0x91, 0x02,                    //   OUTPUT (Data,Var,Abs)
    0x95, 0x01,                    //   REPORT_COUNT (1)
    0x75, 0x03,                    //   REPORT_SIZE (3)
    0x91, 0x03,                    //   OUTPUT (Cnst,Var,Abs)
    0x95, 0x06,                    //   REPORT_COUNT (6)
    0x75, 0x08,                    //   REPORT_SIZE (8)
    0x15, 0x00,                    //   LOGICAL_MINIMUM (0)
    0x25, 0x65,                    //   LOGICAL_MAXIMUM (101)
    0x05, 0x07,                    //   USAGE_PAGE (Keyboard)
    0x19, 0x00,                    //   USAGE_MINIMUM (Reserved (no event indicated))
    0x29, 0x65,                    //   USAGE_MAXIMUM (Keyboard Application)
    0x81, 0x00,                    //   INPUT (Data,Ary,Abs)
    0xc0}                          // End Collection
};

#endif

#ifdef USB_USE_HID_MOUSE
const struct{uint8_t report[HID_RPT01_SIZE];}hid_rpt01=
{
    {
        0x05, 0x01, /* Usage Page (Generic Desktop)             */
        0x09, 0x02, /* Usage (Mouse)                            */
        0xA1, 0x01, /* Collection (Application)                 */
        0x09, 0x01, /*  Usage (Pointer)                         */
        0xA1, 0x00, /*  Collection (Physical)                   */
        0x05, 0x09, /*      Usage Page (Buttons)                */
        0x19, 0x01, /*      Usage Minimum (01)                  */
        0x29, 0x03, /*      Usage Maximum (03)                  */
        0x15, 0x00, /*      Logical Minimum (0)                 */
        0x25, 0x01, /*      Logical Maximum (1)                 */
        0x95, 0x03, /*      Report Count (3)                    */
        0x75, 0x01, /*      Report Size (1)                     */
        0x81, 0x02, /*      Input (Data, Variable, Absolute)    */
        0x95, 0x01, /*      Report Count (1)                    */
        0x75, 0x05, /*      Report Size (5)                     */
        0x81, 0x01, /*      Input (Constant)    ;5 bit padding  */
        0x05, 0x01, /*      Usage Page (Generic Desktop)        */
        0x09, 0x30, /*      Usage (X)                           */
        0x09, 0x31, /*      Usage (Y)                           */
        0x15, 0x81, /*      Logical Minimum (-127)              */
        0x25, 0x7F, /*      Logical Maximum (127)               */
        0x75, 0x08, /*      Report Size (8)                     */
        0x95, 0x02, /*      Report Count (2)                    */
        0x81, 0x06, /*      Input (Data, Variable, Relative)    */
        0xC0, 0xC0  /* End Collection,End Collection            */
    }
};

#endif
#endif
// *****************************************************************************
// *****************************************************************************
// Section: File Scope or Global Constants
// *****************************************************************************
// *****************************************************************************
#if defined(USB_USE_HID_MOUSE) || defined(USB_USE_HID_KEYBOARD)
/* Device Descriptor */
const USB_DEVICE_DESCRIPTOR device_dsc=
{
    0x12,    // Size of this descriptor in bytes
    USB_DESCRIPTOR_DEVICE,                // DEVICE descriptor type
    0x0110,                 // USB Spec Release Number in BCD format
    0x00,                   // Class Code
    0x00,                   // Subclass code
    0x00,                   // Protocol code
    USB_EP0_BUFF_SIZE,          // Max packet size for EP0, see usb_config.h
    MY_VID,                 // Vendor ID
    MY_PID,                 // Product ID: Keyboard fw demo
    FW_VER_BCD,//0x0003,                 // Device release number in BCD format
    0x01,                   // Manufacturer string index
    0x02,                   // Product string index
    0x00,                   // Device serial number string index
    0x01                    // Number of possible configurations
};
#endif



#if defined(USB_USE_HID_MOUSE) && defined(USB_USE_HID_KEYBOARD)
const uint8_t configDescriptor1[]={ 
  /* Configuration Descriptor */
    0x09,//sizeof(USB_CFG_DSC),    // Size of this descriptor in bytes
    USB_DESCRIPTOR_CONFIGURATION,                // CONFIGURATION descriptor type
    DESC_CONFIG_WORD(0x0042),  //YTS  // Total length of data for this cfg
    2,   //YTS                   // Number of interfaces in this cfg
    1,                      // Index value of this configuration
    0,                      // Configuration string index
    _DEFAULT | _SELF,               // Attributes, see usb_device.h
    50,                     // Max power consumption (2X mA)

    /* Interface Descriptor1 */
    0x09,//sizeof(USB_INTF_DSC),   // Size of this descriptor in bytes
    USB_DESCRIPTOR_INTERFACE,               // INTERFACE descriptor type
    0,                      // Interface Number
    0,                      // Alternate Setting Number
    1,                      // Number of endpoints in this intf
    HID_INTF,               // Class code
    BOOT_INTF_SUBCLASS,     // Subclass code
    HID_PROTOCOL_MOUSE,     // Protocol code
    4,                      // Interface string index

    /* HID Class-Specific Descriptor */
    0x09,//sizeof(USB_HID_DSC)+3,    // Size of this descriptor in bytes RRoj hack
    DSC_HID,                // HID descriptor type
    DESC_CONFIG_WORD(0x0111),                 // HID Spec Release Number in BCD format (1.11)
    0x00,                   // Country Code (0x00 for Not supported)
    HID_NUM_OF_DSC,         // Number of class descriptors, see usbcfg.h
    DSC_RPT,                // Report descriptor type
    DESC_CONFIG_WORD(50),   //sizeof(hid_rpt01),      // Size of the report descriptor
    
    /* Endpoint Descriptor */
    0x07,/*sizeof(USB_EP_DSC)*/
    USB_DESCRIPTOR_ENDPOINT,    //Endpoint Descriptor
    HID_EP1 | _EP_IN,  //YTS          //EndpointAddress
    _INTERRUPT,                       //Attributes
    DESC_CONFIG_WORD(3),                  //size
    0x01,                        //Interval


//YTS ADD
 /* Interface Descriptor2 */
    0x09,//sizeof(USB_INTF_DSC),   // Size of this descriptor in bytes
    USB_DESCRIPTOR_INTERFACE,               // INTERFACE descriptor type
    1, //YTS                     // Interface Number
    0,                      // Alternate Setting Number
    1,                      // Number of endpoints in this intf
    HID_INTF,               // Class code
    BOOT_INTF_SUBCLASS,     // Subclass code
    HID_PROTOCOL_KEYBOARD,     // Protocol code
    5,                      // Interface string index

    /* HID Class-Specific Descriptor */
    0x09,//sizeof(USB_HID_DSC)+3,    // Size of this descriptor in bytes RRoj hack
    DSC_HID,                // HID descriptor type
    DESC_CONFIG_WORD(0x0111),                 // HID Spec Release Number in BCD format (1.11)
    0x00,                   // Country Code (0x00 for Not supported)
    HID_NUM_OF_DSC,         // Number of class descriptors, see usbcfg.h
    DSC_RPT,                // Report descriptor type
    DESC_CONFIG_WORD(63),   //sizeof(hid_rpt02),      // Size of the report descriptor

    /* Endpoint Descriptor */
    0x07,/*sizeof(USB_EP_DSC)*/
    USB_DESCRIPTOR_ENDPOINT,    //Endpoint Descriptor
    HID_EP2 | _EP_IN,            //EndpointAddress
    _INTERRUPT,                       //Attributes
    DESC_CONFIG_WORD(8),        //size
    0x01,                        //Interval

    /* Endpoint Descriptor */
    0x07,/*sizeof(USB_EP_DSC)*/
    USB_DESCRIPTOR_ENDPOINT,    //Endpoint Descriptor
    HID_EP2 | _EP_OUT,            //EndpointAddress
    _INTERRUPT,                       //Attributes
    DESC_CONFIG_WORD(8),        //size
    0x01                        //Interval

 };   
 //Language code string descriptor
const struct{uint8_t bLength;uint8_t bDscType;uint16_t string[1];}sd000={
sizeof(sd000),USB_DESCRIPTOR_STRING,{0x0409}
};


//Manufacturer string descriptor
const struct{uint8_t bLength;uint8_t bDscType;uint16_t string[8];} sd001=  {
sizeof(sd001),USB_DESCRIPTOR_STRING,{'K','-','T','r','o','n','i','c'}
};   

//const struct{uint8_t bLength;uint8_t bDscType;uint16_t string[39]; } sd002 = {
//	sizeof(sd002),USB_DESCRIPTOR_STRING,
//	{'U','S','B',' ','M','o','u','s','e',' ','J','o','y','s','t',
//	'i','c','k','/','K','e','y','b','o','a','r','d',' ','K','-','t','r','o','n','i','c'}
//};
const struct{uint8_t bLength;uint8_t bDscType;uint16_t string[21]; } sd002 = {
	sizeof(sd002),USB_DESCRIPTOR_STRING,{'U','S','B',' ','K','e','y','b','o','a','r','d',' ','K','-','T','r','o','n','i','c'}
};    

const struct{uint8_t bLength;uint8_t bDscType;uint16_t string[5]; } sd003 = {
	sizeof(sd003),USB_DESCRIPTOR_STRING,{'V','2','.','2','9'}
};

const struct{uint8_t bLength;uint8_t bDscType;uint16_t string[8]; } sd004 = {
	sizeof(sd004),USB_DESCRIPTOR_STRING,{'K','e','y','b','o','a','r','d'}
};


const struct{uint8_t bLength;uint8_t bDscType;uint16_t string[8]; } sd005 = {
	sizeof(sd005),USB_DESCRIPTOR_STRING,{'K','e','y','b','o','a','r','d'}
};

//Array of configuration descriptors
const uint8_t *const USB_CD_Ptr[]=
{
    (const uint8_t *const)&configDescriptor1
};

//Array of string descriptors
const uint8_t *const USB_SD_Ptr[USB_NUM_STRING_DESCRIPTORS]=
{
    (const uint8_t *const)&sd000,
    (const uint8_t *const)&sd001,
    (const uint8_t *const)&sd002,
    (const uint8_t *const)&sd003,
    (const uint8_t *const)&sd004,
    (const uint8_t *const)&sd005
};

#else

#ifdef USB_USE_HID_MOUSE

/* Configuration 1 Descriptor */
const uint8_t configDescriptor1[]={
    /* Configuration Descriptor */
    0x09,//sizeof(USB_CFG_DSC),    // Size of this descriptor in bytes
    USB_DESCRIPTOR_CONFIGURATION,                // CONFIGURATION descriptor type
    DESC_CONFIG_WORD(0x0022),   // Total length of data for this cfg
    1,                      // Number of interfaces in this cfg
    1,                      // Index value of this configuration
    0,                      // Configuration string index
    _DEFAULT | _SELF,               // Attributes, see usb_device.h
    50,                     // Max power consumption (2X mA)

    /* Interface Descriptor */
    0x09,//sizeof(USB_INTF_DSC),   // Size of this descriptor in bytes
    USB_DESCRIPTOR_INTERFACE,               // INTERFACE descriptor type
    0,                      // Interface Number
    0,                      // Alternate Setting Number
    1,                      // Number of endpoints in this intf
    HID_INTF,               // Class code
    BOOT_INTF_SUBCLASS,     // Subclass code
    HID_PROTOCOL_MOUSE,     // Protocol code
    0,                      // Interface string index

    /* HID Class-Specific Descriptor */
    0x09,//sizeof(USB_HID_DSC)+3,    // Size of this descriptor in bytes RRoj hack
    DSC_HID,                // HID descriptor type
    DESC_CONFIG_WORD(0x0111),                 // HID Spec Release Number in BCD format (1.11)
    0x00,                   // Country Code (0x00 for Not supported)
    HID_NUM_OF_DSC,         // Number of class descriptors, see usbcfg.h
    DSC_RPT,                // Report descriptor type
    DESC_CONFIG_WORD(50),   //sizeof(hid_rpt01),      // Size of the report descriptor
    
    /* Endpoint Descriptor */
    0x07,/*sizeof(USB_EP_DSC)*/
    USB_DESCRIPTOR_ENDPOINT,    //Endpoint Descriptor
    HID_EP | _EP_IN,            //EndpointAddress
    _INTERRUPT,                 //Attributes
    DESC_CONFIG_WORD(3),        //size
    0x01                        //Interval
    
   
};
//Language code string descriptor
const struct{uint8_t bLength;uint8_t bDscType;uint16_t string[1];}sd000={
sizeof(sd000),USB_DESCRIPTOR_STRING,{0x0409}
};

//Manufacturer string descriptor
const struct{uint8_t bLength;uint8_t bDscType;uint16_t string[25];}sd001={
sizeof(sd001),USB_DESCRIPTOR_STRING,
{'M','i','c','r','o','c','h','i','p',' ',
'T','e','c','h','n','o','l','o','g','y',' ','I','n','c','.'}
};
//Product string descriptor
const struct{uint8_t bLength;uint8_t bDscType;uint16_t string[22];}sd002={
sizeof(sd002),USB_DESCRIPTOR_STRING,
{'M','o','u','s','e',' ','I','n',' ','a',' ',
'S','q','u','a','r','e',' ','D','e','m','o'
}};
const uint8_t *const USB_CD_Ptr[]=
{
    (const uint8_t *const)&configDescriptor1
};

//Array of string descriptors
const uint8_t *const USB_SD_Ptr[]=
{
    (const uint8_t *const)&sd000,
    (const uint8_t *const)&sd001,
    (const uint8_t *const)&sd002
};

#endif
#ifdef USB_USE_HID_KEYBOARD
/* Configuration 1 Descriptor */
const uint8_t configDescriptor1[]={
    /* Configuration Descriptor */
    0x09,//sizeof(USB_CFG_DSC),    // Size of this descriptor in bytes
    USB_DESCRIPTOR_CONFIGURATION,                // CONFIGURATION descriptor type
    DESC_CONFIG_WORD(0x0029),   // Total length of data for this cfg
    1,                      // Number of interfaces in this cfg
    1,                      // Index value of this configuration
    0,                      // Configuration string index
    _DEFAULT | _SELF,               // Attributes, see usb_device.h
    50,                     // Max power consumption (2X mA)

    /* Interface Descriptor */
    0x09,//sizeof(USB_INTF_DSC),   // Size of this descriptor in bytes
    USB_DESCRIPTOR_INTERFACE,               // INTERFACE descriptor type
    0,                      // Interface Number
    0,                      // Alternate Setting Number
    1,//2                      // Number of endpoints in this intf
    HID_INTF,               // Class code
    BOOT_INTF_SUBCLASS,     // Subclass code
    HID_PROTOCOL_KEYBOARD,     // Protocol code
    0,                      // Interface string index

    /* HID Class-Specific Descriptor */
    0x09,//sizeof(USB_HID_DSC)+3,    // Size of this descriptor in bytes RRoj hack
    DSC_HID,                // HID descriptor type
    DESC_CONFIG_WORD(0x0111),                 // HID Spec Release Number in BCD format (1.11)
    0x00,                   // Country Code (0x00 for Not supported)
    HID_NUM_OF_DSC,         // Number of class descriptors, see usbcfg.h
    DSC_RPT,                // Report descriptor type
    DESC_CONFIG_WORD(63),   //sizeof(hid_rpt01),      // Size of the report descriptor
    
    /* Endpoint Descriptor */
    0x07,/*sizeof(USB_EP_DSC)*/
    USB_DESCRIPTOR_ENDPOINT,    //Endpoint Descriptor
    HID_EP2 | _EP_IN,            //EndpointAddress
    _INTERRUPT,                       //Attributes
    DESC_CONFIG_WORD(8),        //size
    0x01,                        //Interval

    /* Endpoint Descriptor */
    0x07,/*sizeof(USB_EP_DSC)*/
    USB_DESCRIPTOR_ENDPOINT,    //Endpoint Descriptor
    HID_EP2 | _EP_OUT,            //EndpointAddress
    _INTERRUPT,                       //Attributes
    DESC_CONFIG_WORD(8),        //size
    0x01                        //Interval

};


//Language code string descriptor
const struct{uint8_t bLength;uint8_t bDscType;uint16_t string[1];}sd000={
sizeof(sd000),USB_DESCRIPTOR_STRING,{0x0409}
};


//Manufacturer string descriptor
const struct{uint8_t bLength;uint8_t bDscType;uint16_t string[8];} sd001=  {
sizeof(sd001),USB_DESCRIPTOR_STRING,{'K','-','T','r','o','n','i','c'}
};   

//const struct{uint8_t bLength;uint8_t bDscType;uint16_t string[39]; } sd002 = {
//	sizeof(sd002),USB_DESCRIPTOR_STRING,
//	{'U','S','B',' ','M','o','u','s','e',' ','J','o','y','s','t',
//	'i','c','k','/','K','e','y','b','o','a','r','d',' ','K','-','t','r','o','n','i','c'}
//};
const struct{uint8_t bLength;uint8_t bDscType;uint16_t string[21]; } sd002 = {
	sizeof(sd002),USB_DESCRIPTOR_STRING,{'U','S','B',' ','K','e','y','b','o','a','r','d',' ','K','-','T','r','o','n','i','c'}
};    

const struct{uint8_t bLength;uint8_t bDscType;uint16_t string[5]; } sd003 = {
	sizeof(sd003),USB_DESCRIPTOR_STRING,{'V','2','.','2','9'}
};

const struct{uint8_t bLength;uint8_t bDscType;uint16_t string[8]; } sd004 = {
	sizeof(sd004),USB_DESCRIPTOR_STRING,{'K','e','y','b','o','a','r','d'}
};


const struct{uint8_t bLength;uint8_t bDscType;uint16_t string[8]; } sd005 = {
	sizeof(sd005),USB_DESCRIPTOR_STRING,{'K','e','y','b','o','a','r','d'}
};

//Array of configuration descriptors
const uint8_t *const USB_CD_Ptr[]=
{
    (const uint8_t *const)&configDescriptor1
};

//Array of string descriptors
const uint8_t *const USB_SD_Ptr[USB_NUM_STRING_DESCRIPTORS]=
{
    (const uint8_t *const)&sd000,
    (const uint8_t *const)&sd001,
    (const uint8_t *const)&sd002,
    (const uint8_t *const)&sd003,
    (const uint8_t *const)&sd004,
    (const uint8_t *const)&sd005
};
#endif






#endif






// *****************************************************************************
// *****************************************************************************
// Section: File Scope Data Types
// *****************************************************************************
// *****************************************************************************


// *****************************************************************************
// *****************************************************************************
// Section: Macros or Functions
// *****************************************************************************
// *****************************************************************************

#if defined(__18CXX)
    #pragma code
#endif

#endif
/** EOF usb_descriptors.c ****************************************************/

/*******************************************************************************
 End of File
*/