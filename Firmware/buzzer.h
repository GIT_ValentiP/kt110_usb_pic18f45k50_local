/* 
 * File:   buzzer.h
 * Author: firmware
 *
 * Created on 3 marzo 2021, 16.59
 */

#ifndef BUZZER_H
#define	BUZZER_H

#ifdef	__cplusplus
extern "C" {
#endif
    
#include "globals.h"
#include "mcc_generated_files/epwm1.h"
#include "mcc_generated_files/tmr2.h"
/***** PROTOTYPES ***/    
    
void Init_Buzzer(void);   
void Init_Chiave(void);   
void Set_Buzzer(uint16_t durata,uint8_t nbeep);
void Buz_manager(void);
void Chiave_manager(void);


#ifdef	__cplusplus
}
#endif

#endif	/* BUZZER_H */

