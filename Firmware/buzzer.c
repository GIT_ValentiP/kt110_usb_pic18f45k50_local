/**
  Generated Main Source File

  Company:
 K-Tronic

  File Name:
    buzzer.c

  Summary:
  

  Description:
    This header file provides implementations for driver 
*/

/**
  Section: Included Files
*/
#include "buzzer.h"


/**
  Section: Global Variables Definitions
*/

/**
  Section: Buzzer APIs
*/

void Init_Buzzer(void){
duty_pwm_backlight=50;
EPWM1_LoadDutyValue(duty_pwm_backlight);    
#ifdef BUZZER_ON
    buz1.active=1;
    
#else
    buz1.active=0;    
#endif
    TMR2_StopTimer(); // Stop TMR2     
    buz1.count_down_10ms=0; 
    buz1.t_off_10ms=0;
    buz1.t_on_10ms=0;
    buz1.nbeep=0;
    buz1.period_10ms=0;
    buz1.start=0;
    
}   


void Set_Buzzer(uint16_t durata,uint8_t nbeep){
  
    buz1.t_off_10ms=(durata/2);
    buz1.t_on_10ms=durata;
    buz1.nbeep=nbeep-1;
    buz1.period_10ms=durata;
    buz1.start=1; 
}

void Buz_manager(void){
   if(buz1.active==1){
       if(buz1.start==1){   
          buz1.count_down_10ms=buz1.period_10ms;  
          TMR2_StartTimer(); // Start TMR2          
          buz1.start=0;
          buz1.start_off=0;
       }
       if((buz1.count_down_10ms ==0)&&(buz1.start_off==0)){
           TMR2_StopTimer(); // Stop TMR2     
           if(buz1.nbeep>0){
               buz1.start_off=1;
               buz1.t_off_10ms=(buz1.period_10ms/2); 
              //buz1.count_down_10ms=buz1.period_10ms;  
              
              //buz1.nbeep--; 
           }
       }
       if((buz1.t_off_10ms ==0)&&(buz1.start_off==1)){
           TMR2_StopTimer(); // Stop TMR2     
           if(buz1.nbeep>0){
              //buz1.t_off_10ms=(buz1.period_10ms/2);             
              buz1.nbeep--; 
              buz1.start=1;
              buz1.start_off=0;
           }
       }    
   } 
}

/**
  Section: Chiave APIs
*/


void Init_Chiave(void){

 chiave.btn0_sel1=0;  //Type Chiave Button:0   Selector:1  
#ifdef CHIAVE_ON
    chiave.active=1;
    #ifdef CHIAVE_SEL
      chiave.btn0_sel1=1;
    #endif
    chiave.status   =CHIAVE_START_VALUE;
#else
    chiave.active=0;   
    chiave.status   =KEYBOARD_UNLOCK;
#endif
    chiave.logic    =CHIAVE_LOGIC;//logic=1=positive pressed=1  release=0   logic=0=negative pressed=0  release=1
    chiave.actual   =SW_CHIAVE_GetValue();//KLED_CHIAVE_GetValue();
    chiave.del_start=0;
   
    chiave.old=chiave.actual;
    chiave.deb=0;
} 

void Chiave_manager(void){
   if(chiave.active==1){    
       chiave.old=chiave.actual;
       chiave.actual=SW_CHIAVE_GetValue();//KLED_CHIAVE_GetValue();
       if(chiave.btn0_sel1==1){   //Selettore Bistabile
        
         if(chiave.actual==chiave.logic){
              chiave.status=KEYBOARD_UNLOCK;
         }else{
              chiave.status=KEYBOARD_LOCK;
         }
         if(chiave.actual!=chiave.old){
          Set_Buzzer(BEEP_SHORT_PERIOD,1);
         } 
       }else{//Pulsante - monostabile -
            if(chiave.actual!=chiave.old){
                if(chiave.actual==chiave.logic){//button pressed
                   Set_Buzzer(BEEP_SHORT_PERIOD,1); 
                   chiave.status=(chiave.status==0?1:0);   
                   chiave.deb=0;
                }else{//button released
                    
                }
            }else{
                 if(chiave.actual==chiave.logic){//button pressed
                   chiave.deb++;
                 }else{
                   chiave.deb=0;  
                 }  
            }   
       }
       if(chiave.status==KEYBOARD_UNLOCK){
          KLED_CHIAVE_SetLow();  //Turn OFF
       }else{        
          KLED_CHIAVE_SetHigh();  //Turn On LED
       }
   }else{
       KLED_CHIAVE_SetLow();
   } 
}