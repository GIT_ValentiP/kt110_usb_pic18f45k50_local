/**
  Generated Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This is the main file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.78
        Device            :  PIC18F45K50
        Driver Version    :  2.00
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#include "globals.h"
#include "keyboard_mouse_manager.h"
#include "mcc_generated_files/mcc.h"
#include "buzzer.h"

/**
  Section: Macro Declarations
*/

//#define PWM1_INITIALIZE_DUTY_VALUE    149
/*
                         Main application
 */
void main(void)
{
    // Initialize the device
    WDTCONbits.SWDTEN = 1;      /* enable software controlled watchdog timer*/
    CLRWDT();  
    SYSTEM_Initialize();
    CLRWDT();  
    KLED_SCROLL_SetHigh(); 
    KLED_CAPS_SetHigh();  
    KLED_NUM_SetHigh();
    KLED_CHIAVE_SetHigh();
    Init_Chiave();
    Init_Buzzer();
    
    TMR0_StartTimer();// Start TMR0 System Timer
    
    
    // If using interrupts in PIC18 High/Low Priority Mode you need to enable the Global High and Low Interrupts
    // If using interrupts in PIC Mid-Range Compatibility Mode you need to enable the Global and Peripheral Interrupts
    // Use the following macros to:

    // Enable the Global Interrupts
    INTERRUPT_GlobalInterruptEnable();

    // Disable the Global Interrupts
    //INTERRUPT_GlobalInterruptDisable();

    // Enable the Peripheral Interrupts
    INTERRUPT_PeripheralInterruptEnable();

    // Disable the Peripheral Interrupts
    //INTERRUPT_PeripheralInterruptDisable();

    
    CLRWDT();  
    while (1)
    { 
        // KeyboardScanRead();
        // Add your application code
        CLRWDT();  
        if (main_clock.flag_10ms==1){  
            KeyboardScanRead();
            Chiave_manager();
            //Handle_MouseUSB();
            SetLeds();
            APP_KeyboardTasks();
            main_clock.flag_10ms=0;
        }
        Buz_manager();
        if (main_clock.flag_100ms==1){  
            //KeyboardScanRead(); 
            main_clock.flag_100ms=0;
        }
        
        if (main_clock.flag_1sec==1){
            //KLED_CHIAVE_Toggle(); 
            main_clock.flag_1sec=0;
        }
        
        if (main_clock.flag_5sec==1){         
            main_clock.flag_5sec=0;
        }
        
        if (main_clock.flag_10sec==1){
          
            main_clock.flag_10sec=0;
        }
        
        if (main_clock.flag_1min==1){    
            main_clock.flag_1min=0;
        }
        // EPWM1_LoadDutyValue(duty_pwm_backlight);
        /* Run the keyboard demo tasks. */
        
        // APP_DeviceMouseTasks();
    }   
        
}
/**
 End of File
*/