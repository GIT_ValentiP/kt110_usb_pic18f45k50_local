/* 
 * File:   globals.h
 * Author: firmware
 *
 * Created on 17 dicembre 2019, 13.50
 */

#ifndef GLOBALS_H
#define	GLOBALS_H
#include <xc.h>
#include "mcc_generated_files/device_config.h"
#include "mcc_generated_files/pin_manager.h"
#include <stdint.h>
#include <stdbool.h>

#ifdef	__cplusplus
extern "C" {
#endif

/********* DEFINE ********/
#define FW_VER_BCD 0x0103    
#define FW_VER  0x01
#define FW_SUB  0x03   
#define USB_USE_HID_KEYBOARD
//#define USB_USE_HID_MOUSE    
//#define USB_USE_HID_KEYBOARD_MULTIMEDIA    
    
 /* Configuration */


#define KEYBOARD_UNLOCK     1
#define KEYBOARD_LOCK       0    
#define LOGIC_POS           1
#define LOGIC_NEG           0    
    
#define BUZZER_ON     1  //COMMENT if BUZZER is UNABLED
#define CHIAVE_ON     1  //COMMENT if CHIAVE is UNABLED
//#define CHIAVE_SEL  1  //UNCOMMENT if CHIAVE is a BISTABLE SELECTOR //COMMENT if CHIAVE is a BUTTON
                         
#define CHIAVE_START_VALUE  KEYBOARD_UNLOCK       
#define CHIAVE_LOGIC        LOGIC_POS
    
    
    
    
#define ROTATE_MOUSE      0
#define LEFT_BUTTON_BIT   0				// per USB (letto su mouse Cypress!)
#define RIGHT_BUTTON_BIT  1	
#define MIDDLE_BUTTON_BIT 2

#define DELAY_BASE  40	// 212uS	
#define MOV_THRS_OK	2

#define LARIMART_RETROILLUMINAZIONE 1		// su RC2, buzzer via!
#define LARIMART_RETROILLUMINAZIONE_STATO_INIZIALE 2	// 0=stealth, 1=non-stealth, 2=non-stealth ma retroill. spenta
#define CONTROLLA_PHANTOM 1
#define CONTROLLA_PHANTOM_7 1
#define SCANCNT_M_DEFAULT 30			// default rate scan/sec mouse
#define SCANCNT_K_DEFAULT 50			// default rate scan/sec tastiera
#define SCANCNT_EVERY_2_IDLE_DEFAULT_K 8	// ora fa una scan tastiera ogni 70mS, l'idle dev'essere ogni 500mSec
#define SCANCNT_EVERY_2_IDLE_DEFAULT_M 8	// ?? che ci facciamo

#define TRIGM			0                //TRIGGER FLAG mouse update
#define TRIGK			1                //TRIGGER FLAG kb update
#define TRIGK2		    2                //TRIGGER FLAG keyboard update vuoto (tutti zeri)

#define MAX_TASTI_CONTEMPORANEI  4
 
    
#define NUM_ROW_MAX      11
#define NUM_COL_MAX      10
#define NUM_BUTTON_MOUSE  2   
    
#define OUTPUT_PIN_PORT 0
#define INPUT_PIN_PORT  1    

#define KEY_PRESSED_VALUE 0    
#define KEY_PRESSED_DEBOUNCE_MS 3//10    
    
    
#define KEYBOARD_TIMEOUT_IDLE 50000 //in milliseconds    
    
    
#define BEEP_SHORT_PERIOD  8//20 //in 10ms    
#define BEEP_SHORT_TOFF     5 //in 10ms       
/*******  VARIABLES KEYBOARD AND MOUSE MANAGEMENT ********/  
uint8_t mouseBuffer[8],kbBufferO[8],kbKeys[MAX_TASTI_CONTEMPORANEI];
uint8_t tmr_comp;
uint8_t counter_6mS;
uint8_t scanFreqK,scanFreqM;		// quante scansioni/sec per il mouse/tastiera
uint8_t scanIdleCntK,scanIdleCntM	/*usare 14/10/08*/;

uint8_t Leds;
uint8_t kbScanCode;
uint8_t BSTAT;
uint8_t FLAGA;        //GENERAL PURPOSE FLAG
uint8_t FLAGS;
uint8_t XCOUNT;		 /*X-MOVEMENT COUNTER*/
uint8_t YCOUNT		 /*Y-MOVEMENT COUNTER*/;
uint8_t XVel	/* Vx*/,YVel /* Vy*/;
uint8_t XVel2,YVel2;
uint8_t READ[4];			//Formatted data read from inputs
				//X-movement since last report
				//Y-movement since last report
				//Used for MS intellimouse data reporting
uint8_t LENGTH;
uint8_t oldREAD1;
uint8_t kbLayout,kbLayoutTemp;
uint8_t RCALC;				//Used for calculating Report Rate
uint8_t SRATE;				//Sampling Rate
uint8_t RESN;					//Resolution
uint8_t SCALING;			//Used for movement distance adjustment
uint8_t ID;						//Current device ID
uint8_t STATE;				//Used for accepting "extended" commands
uint8_t Mode;

uint16_t rowState[10];

/******* VARIABLES KEYBOARD ********/    

typedef struct
{
    //uint8_t pin;
    //uint8_t port;
    uint8_t val;
    uint16_t deb_ms;
    uint8_t old;
    //uint8_t chr;
} keys_keyboard_t;

typedef struct
{
    uint8_t  XCOUNT;		 /*X-MOVEMENT COUNTER*/
    uint8_t  YCOUNT;		 /*Y-MOVEMENT COUNTER*/
    uint8_t  btn_val[NUM_BUTTON_MOUSE];
    uint16_t btn_deb_ms[NUM_BUTTON_MOUSE];
    uint8_t  btn_old[NUM_BUTTON_MOUSE];  
} mouse_t;

 typedef struct
 {   
        uint8_t numLock        :1;
        uint8_t capsLock       :1;
        uint8_t scrollLock     :1;
        uint8_t compose        :1;
        uint8_t kana           :1;
        uint8_t                :3;
 }keys_leds_t;

 typedef struct
 {   
        uint8_t active              :1;
        uint8_t start               :1;
        uint8_t start_off           :1;
        uint8_t nbeep               :5;
        uint16_t period_10ms;
        uint16_t count_down_10ms;
        uint16_t t_on_10ms;
        uint16_t t_off_10ms;
 }buzzer_t;
 
  typedef struct
 {   
        uint8_t active                    :1;
        uint8_t btn0_sel1                 :1;
        uint8_t logic                     :1;
        uint8_t status                    :1;
        uint8_t actual                    :1;
        uint8_t old                       :1;
        uint8_t free                      :2;
        uint16_t deb;
        uint16_t del_start;//
 }switch_t;
 keys_keyboard_t kb_status[NUM_ROW_MAX][NUM_COL_MAX];   
 mouse_t mouse_status;  
 uint8_t num_keys_pressed;   
 uint8_t keys_multi[MAX_TASTI_CONTEMPORANEI];
 uint8_t keypad_multi[MAX_TASTI_CONTEMPORANEI];
 keys_leds_t keys_special;
 uint8_t  id_kp;
 buzzer_t buz1;
 switch_t chiave;
/******* VARIABLES SYSTEM TIMER********/    
uint16_t duty_pwm_backlight;
typedef struct
{
    uint16_t us100;
    uint16_t ms;
    uint16_t ms10;        
    uint16_t ms100;
    uint8_t  sec;
    uint8_t  min;
    uint32_t hour;
    bool flag_100us;
    bool flag_10ms;
    bool flag_100ms;
    bool flag_1sec;
    bool flag_1min;
    bool flag_5sec;
    bool flag_10sec;
} system_time_struct_t;

uint16_t keyboardIdleRateMillisec;
uint8_t  scan_read_period_ms;
uint8_t  scan_row_delay;
system_time_struct_t main_clock;
#ifdef	__cplusplus
}
#endif

#endif	/* GLOBALS_H */

